# OGPToolbox-API

HTTP API for OGPToolbox database

## More informations

* [OGPToolbox](https://ogptoolbox.org)
* [Open Software Base project](https://git.framasoft.org/codegouv/open-software-base-yaml)

## Installation

For the first time only:

```bash
npm install
```

### Configure the API server

```bash
npm run configure
```

### Launch the API server

```bash
npm run start
```

## API

To explore OGPToolbox API, open [sample Swagger UI](http://petstore.swagger.io/) and explore http://localhost:3020/swagger.json.

## API usage

### API usage for users

#### Create a user

```bash
cat <<'EOF' | curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" --data-binary @- "http://localhost:3000/users"
{
  "name": "Alice",
  "urlName": "alice",
  "password": "secret"
}
EOF
```

#### Login to retrieve user API key

```bash
cat <<'EOF' | curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" --data-binary @- "http://localhost:3000/login"
{
  "userName": "alice",
  "password": "secret"
}
EOF
```

Returns:

```json
{
  "apiVersion": "1",
  "data": {
    "apiKey": "HoIw4IqGwymIeP+xRK2MUg",
    "createdAt": "2016-05-03T21:28:34.447Z",
    "name": "Alice",
    "urlName": "alice"
  }
}
```

Retrieve the API key in field `data.apiKey` of the response.

### API usage for tools

TODO
