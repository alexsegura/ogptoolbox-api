// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


export default {
  contact: {
    // email:
    name: "OGPToolbox-API Team",
    // url:
  },
  db: {
    host: "localhost",
    name: "ogptoolbox",
    port: 28015,
  },
  description: "API for the OGPToolbox database",
  host: "localhost",
  keys: [
    // Keys for Keygrip <https://github.com/crypto-utils/keygrip>, used by signed cookie keys, etc
    "OGPToolbox-API not very secret key, to override",
  ],
  license: {
    // API license (not software license)
    name: "MIT",
    url: "http://opensource.org/licenses/MIT",
  },
  listen: {
    host: null,  // Listen to every IPv4 addresses.
    port: null,  // Listen to config.port by default
  },
  port: 3020,
  proxy: false,  // Is this application used behind a trusted proxy?
  title: "OGPToolbox-API",
}
