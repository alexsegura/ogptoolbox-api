// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"


export {createTag}
async function createTag(ctx) {
  let tag = ctx.parameter.tag

  tag.updatedAt = r.now()
  delete tag.id

  let result = await r
    .table("tags")
    .insert(tag, {returnChanges: true})
  tag = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toTagJson(tag),
  }
}


export {deleteTag}
async function deleteTag(ctx) {
  // Delete an existing tag.
  let tag = ctx.tag

  tag.deleted = true
  // TODO: If delete is kept, also remove all other linked objects (tools, etc).
  await r
    .table("tags")
    .get(tag.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toTagJson(tag),
  }
}


export {getTag}
async function getTag(ctx) {
  // Respond an existing tag.
  ctx.body = {
    apiVersion: "1",
    data: toTagJson(ctx.tag),
  }
}


export {listTags}
async function listTags(ctx) {
  let tags = await r.table("tags")
    .orderBy({index: r.desc("updatedAt")})

  ctx.body = {
    apiVersion: "1",
    data: tags.map(tag => toTagJson(tag)),
  }
}


export {requireTag}
async function requireTag(ctx, next) {
  let id = ctx.parameter.tagId
  let tag = await r
    .table("tags")
    .get(id)
  if (tag === null) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No tag with ID "${id}".`,
    }
    return
  }
  ctx.tag = tag

  await next()
}


function toTagJson(tag) {
  let tagJson = {...tag}
  tagJson.updatedAt = tagJson.updatedAt.toISOString()
  return tagJson
}


export {updateTag}
async function updateTag(ctx) {
  // Update an existing tag.
  let tag = ctx.parameter.tag

  tag.id = ctx.tag.id
  tag.updatedAt = r.now()

  let result = await r
    .table("tags")
    .get(tag.id)
    .replace(tag, {returnChanges: true})
  tag = result.changes[0].new_val
  ctx.body = {
    apiVersion: "1",
    data: toTagJson(tag),
  }
}
