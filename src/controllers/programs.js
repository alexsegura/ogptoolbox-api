// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"


export {createProgram}
async function createProgram(ctx) {
  let program = ctx.parameter.program

  program.updatedAt = r.now()
  delete program.id

  let result = await r
    .table("programs")
    .insert(program, {returnChanges: true})
  program = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toProgramJson(program),
  }
}


export {deleteProgram}
async function deleteProgram(ctx) {
  // Delete an existing program.
  let program = ctx.program

  program.deleted = true
  // TODO: If delete is kept, also remove all other linked objects (tools, etc).
  await r
    .table("programs")
    .get(program.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toProgramJson(program),
  }
}


export {getProgram}
async function getProgram(ctx) {
  // Respond an existing program.
  ctx.body = {
    apiVersion: "1",
    data: toProgramJson(ctx.program),
  }
}


export {listPrograms}
async function listPrograms(ctx) {
  let programs = await r.table("programs")
    .orderBy({index: r.desc("updatedAt")})

  ctx.body = {
    apiVersion: "1",
    data: programs.map(program => toProgramJson(program)),
  }
}


export {requireProgram}
async function requireProgram(ctx, next) {
  let id = ctx.parameter.programId
  let program = await r
    .table("programs")
    .get(id)
  if (program === null) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No program with ID "${id}".`,
    }
    return
  }
  ctx.program = program

  await next()
}


function toProgramJson(program) {
  let programJson = {...program}
  programJson.updatedAt = programJson.updatedAt.toISOString()
  return programJson
}


export {updateProgram}
async function updateProgram(ctx) {
  // Update an existing program.
  let program = ctx.parameter.program

  program.id = ctx.program.id
  program.updatedAt = r.now()

  let result = await r
    .table("programs")
    .get(program.id)
    .replace(program, {returnChanges: true})
  program = result.changes[0].new_val
  ctx.body = {
    apiVersion: "1",
    data: toProgramJson(program),
  }
}
