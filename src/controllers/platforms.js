// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"


export {createPlatform}
async function createPlatform(ctx) {
  let platform = ctx.parameter.platform

  platform.updatedAt = r.now()
  delete platform.id

  let result = await r
    .table("platforms")
    .insert(platform, {returnChanges: true})
  platform = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toPlatformJson(platform),
  }
}


export {deletePlatform}
async function deletePlatform(ctx) {
  // Delete an existing platform.
  let platform = ctx.platform

  platform.deleted = true
  // TODO: If delete is kept, also remove all other linked objects (tools, etc).
  await r
    .table("platforms")
    .get(platform.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toPlatformJson(platform),
  }
}


export {getPlatform}
async function getPlatform(ctx) {
  // Respond an existing platform.
  ctx.body = {
    apiVersion: "1",
    data: toPlatformJson(ctx.platform),
  }
}


export {listPlatforms}
async function listPlatforms(ctx) {
  let platforms = await r.table("platforms")
    .orderBy({index: r.desc("updatedAt")})

  ctx.body = {
    apiVersion: "1",
    data: platforms.map(platform => toPlatformJson(platform)),
  }
}


export {requirePlatform}
async function requirePlatform(ctx, next) {
  let id = ctx.parameter.platformId
  let platform = await r
    .table("platforms")
    .get(id)
  if (platform === null) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No platform with ID "${id}".`,
    }
    return
  }
  ctx.platform = platform

  await next()
}


function toPlatformJson(platform) {
  let platformJson = {...platform}
  platformJson.updatedAt = platformJson.updatedAt.toISOString()
  return platformJson
}


export {updatePlatform}
async function updatePlatform(ctx) {
  // Update an existing platform.
  let platform = ctx.parameter.platform

  platform.id = ctx.platform.id
  platform.updatedAt = r.now()

  let result = await r
    .table("platforms")
    .get(platform.id)
    .replace(platform, {returnChanges: true})
  platform = result.changes[0].new_val
  ctx.body = {
    apiVersion: "1",
    data: toPlatformJson(platform),
  }
}
