// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"


export {createOrganization}
async function createOrganization(ctx) {
  let organization = ctx.parameter.organization

  organization.updatedAt = r.now()
  delete organization.id

  let result = await r
    .table("organizations")
    .insert(organization, {returnChanges: true})
  organization = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toOrganizationJson(organization),
  }
}


export {deleteOrganization}
async function deleteOrganization(ctx) {
  // Delete an existing organization.
  let organization = ctx.organization

  organization.deleted = true
  // TODO: If delete is kept, also remove all other linked objects (tools, etc).
  await r
    .table("organizations")
    .get(organization.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toOrganizationJson(organization),
  }
}


export {getOrganization}
async function getOrganization(ctx) {
  // Respond an existing organization.
  ctx.body = {
    apiVersion: "1",
    data: toOrganizationJson(ctx.organization),
  }
}


export {listOrganizations}
async function listOrganizations(ctx) {
  let organizations = await r.table("organizations")
    .orderBy({index: r.desc("updatedAt")})

  ctx.body = {
    apiVersion: "1",
    data: organizations.map(organization => toOrganizationJson(organization)),
  }
}


export {requireOrganization}
async function requireOrganization(ctx, next) {
  let id = ctx.parameter.organizationId
  let organization = await r
    .table("organizations")
    .get(id)
  if (organization === null) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No organization with ID "${id}".`,
    }
    return
  }
  ctx.organization = organization

  await next()
}


function toOrganizationJson(organization) {
  let organizationJson = {...organization}
  organizationJson.updatedAt = organizationJson.updatedAt.toISOString()
  return organizationJson
}


export {updateOrganization}
async function updateOrganization(ctx) {
  // Update an existing organization.
  let organization = ctx.parameter.organization

  organization.id = ctx.organization.id
  organization.updatedAt = r.now()

  let result = await r
    .table("organizations")
    .get(organization.id)
    .replace(organization, {returnChanges: true})
  organization = result.changes[0].new_val
  ctx.body = {
    apiVersion: "1",
    data: toOrganizationJson(organization),
  }
}
