// OGPToolbox-API -- HTTP API for the OGPToolbox database
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-api
//
// OGPToolbox-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"


export {createUsage}
async function createUsage(ctx) {
  let usage = ctx.parameter.usage

  usage.updatedAt = r.now()
  delete usage.id

  let result = await r
    .table("usages")
    .insert(usage, {returnChanges: true})
  usage = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toUsageJson(usage),
  }
}


export {deleteUsage}
async function deleteUsage(ctx) {
  // Delete an existing usage.
  let usage = ctx.usage

  usage.deleted = true
  // TODO: If delete is kept, also remove all other linked objects (tools, etc).
  await r
    .table("usages")
    .get(usage.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toUsageJson(usage),
  }
}


export {getUsage}
async function getUsage(ctx) {
  // Respond an existing usage.
  ctx.body = {
    apiVersion: "1",
    data: toUsageJson(ctx.usage),
  }
}


export {listUsages}
async function listUsages(ctx) {
  let usages = await r.table("usages")
    .orderBy({index: r.desc("updatedAt")})

  ctx.body = {
    apiVersion: "1",
    data: usages.map(usage => toUsageJson(usage)),
  }
}


export {requireUsage}
async function requireUsage(ctx, next) {
  let id = ctx.parameter.usageId
  let usage = await r
    .table("usages")
    .get(id)
  if (usage === null) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No usage with ID "${id}".`,
    }
    return
  }
  ctx.usage = usage

  await next()
}


function toUsageJson(usage) {
  let usageJson = {...usage}
  usageJson.updatedAt = usageJson.updatedAt.toISOString()
  return usageJson
}


export {updateUsage}
async function updateUsage(ctx) {
  // Update an existing usage.
  let usage = ctx.parameter.usage

  usage.id = ctx.usage.id
  usage.updatedAt = r.now()

  let result = await r
    .table("usages")
    .get(usage.id)
    .replace(usage, {returnChanges: true})
  usage = result.changes[0].new_val
  ctx.body = {
    apiVersion: "1",
    data: toUsageJson(usage),
  }
}
